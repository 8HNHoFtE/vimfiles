" :vimgrep /<pattern>/ `git ls-files`
" :1,$s/^M//g
" set t_Co=0
filetype plugin on
filetype plugin indent on


let mapleader=","
if has('terminal')
    if has('vcon')
        set termguicolors
    endif
    "hi Terminal ctermbg=black ctermfg=blue guibg=black guifg=blue
endif


function! Autocommands()
    function! HelpInNewTab()
        if &buftype == 'help'
            execute "normal \<C-W>T"
        endif
    endfunction
    if $env == 'macos'
        autocmd BufNewFile          *           execute('lcd ~/Development/Workspaces')
    endif
    autocmd VimEnter                *           execute('pwd')
    autocmd BufRead,BufNewFile      *.js.dtml   set filetype=javascript
    autocmd BufRead,BufNewFile      *.css.dtml  set filetype=css
    autocmd BufRead,BufNewFile      *.ts        set filetype=typescript
    autocmd BufRead,BufNewFile      *.dart      set filetype=dart
    autocmd BufRead,BufNewFile      *.cr        set filetype=crystal
    autocmd BufWritePre             *           :%s/\s\+$//e
    autocmd FileType                *           execute 'setlocal dict+=$HOME/vimfiles/dict/'.&filetype.'.dic'
    autocmd FileType                gitcommit   setlocal spell spelllang=en_us
    autocmd BufEnter                *.txt       call HelpInNewTab()
    augroup VIMWIKI
        autocmd!
        autocmd FileType            VIMWIKI     syntax on
        autocmd FileType            VIMWIKI     setlocal spell spelllang=en_us
    augroup END
    augroup TEMPLATES
      autocmd BufNewFile            index.html                  0r $HOME/vimfiles/templates/index.html
      autocmd BufNewFile            new_script.py               0r $HOME/vimfiles/templates/skeleton.py
    augroup END
    augroup QUICKFIX
        autocmd QuickFixCmdPost     [^l]*       nested cwindow
        autocmd QuickFixCmdPost     l*          nested lwindow
    augroup END
endfunction


function! WindowsSettings()
    source $VIMRUNTIME/mswin.vim
    behave mswin
    "set renderoptions=type:directx
    autocmd GUIEnter * silent! WToggleClean
    autocmd GUIEnter * silent! WCenter
    autocmd GUIEnter * silent! WSetAlpha 225
    autocmd GUIEnter * silent! syntax on
    nnoremap <silent><F10>                  :WToggleClean<CR>
    nnoremap <silent><F11>                  :WToggleFullscreen<cr>
    nnoremap v <C-Q>

    set pythondll=$HOME\AppData\Local\Programs\Python\Python27\python27.dll
    set pythonthreedll=$HOME\AppData\Local\Programs\Python\Python36\python36.dll


    function MyDiff()
        let opt = '-a --binary '
        if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
        if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
        let arg1 = v:fname_in
        if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
        let arg2 = v:fname_new
        if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
        let arg3 = v:fname_out
        if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
        let eq = ''
        if $VIMRUNTIME =~ ' '
            if &sh =~ '\<cmd'
                let cmd = '""' . $VIMRUNTIME . '\diff"'
                let eq = '"'
            else
                let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
            endif
        else
            let cmd = $VIMRUNTIME . '\diff'
        endif
        silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
    endfunction
    set diffexpr=MyDiff()
endfunction


function! SetRtp()
    set rtp^=$HOME/vimfiles

    " colorschemes
    set rtp^=$HOME/vimfiles/bundle/spacemacs-theme
    set rtp^=$HOME/vimfiles/bundle/vim-sialoquent
    set rtp^=$HOME/vimfiles/bundle/vim-lucius
    set rtp^=$HOME/vimfiles/bundle/vim-colors-synthwave
    set rtp^=$HOME/vimfiles/bundle/nord-vim
    " set rtp^=$HOME/vimfiles/bundle/base16-vim

    " Programming languages
    set rtp^=$HOME/vimfiles/bundle/vim-javascript
    set rtp^=$HOME/vimfiles/bundle/rust
    set rtp^=$HOME/vimfiles/bundle/typescript-vim
    set rtp^=$HOME/vimfiles/bundle/vim-crystal
    " set rtp^=$HOME/vimfiles/bundle/c-support

    " Syntax
    set rtp^=$HOME/vimfiles/bundle/vim-jinja2

    " Vim services
    set rtp^=$HOME/vimfiles/bundle/vim-indent-guides
    set rtp^=$HOME/vimfiles/bundle/vimwiki
    if has("gui_win32")
        set rtp+=$HOME/vimfiles/bundle/wimproved
    endif
endfunction


function! Indent()
    set copyindent
    set smartindent
    set tabstop=8
    set expandtab
    set shiftwidth=4
    set softtabstop=4

    let g:indent_guides_enable_on_vim_startup = 0
    let g:indent_guides_start_level = 2
    let g:indent_guides_guide_size = 1
    let g:indent_guides_auto_colors = 1
endfunction


function! Colorscheme()
    hi Visual term=reverse cterm=reverse
    syntax on
    set background=dark
    colorscheme synthwave
    if has('gui')
        syntax on
    endif
    function! EvoColors()
        if has('gui') == 0
            set background=dark
            colorscheme default
            autocmd BufRead,BufNewFile              *       syntax off
            autocmd FileType                        *       syntax off
        endif
    endfunction
    if $vimenv == 'evo'
        call EvoColors()
    endif
endfunction


function! DarkColorscheme()
    set background=dark
    colorscheme lucius
    function! EvoColors()
        if has('gui') == 0
            set background=dark
            colorscheme default
            autocmd BufRead,BufNewFile              *       syntax off
            autocmd FileType                        *       syntax off
        endif
    endfunction
    if $vimenv == 'evo'
        call EvoColors()
    endif
endfunction


function! LightColorscheme()
    set background=light
    colorscheme lucius
    function! EvoColors()
        if has('gui') == 0
            set background=dark
            colorscheme default
            autocmd BufRead,BufNewFile              *       syntax off
            autocmd FileType                        *       syntax off
        endif
    endfunction
    if $vimenv == 'evo'
        call EvoColors()
    endif
endfunction


function! NordColorscheme()
    set background=dark
    colorscheme nord
    function! EvoColors()
        if has('gui') == 0
            set background=dark
            colorscheme default
            autocmd BufRead,BufNewFile              *       syntax off
            autocmd FileType                        *       syntax off
        endif
    endfunction
    if $vimenv == 'evo'
        call EvoColors()
    endif
endfunction


function! GUI()
    set guiheadroom=0
    set gcr=n:blinkon0
    set antialias
    set mousehide
    set guioptions=Ac
    set guioptions-=T
    set guioptions-=r
    set guioptions-=l
    if has("gui_macvim")
        set transparency=10
        set guifont=Consolas:h13
        set columns=84 lines=50
    endif
    if has("gui_win32")
        set guifont=Consolas:h11
        set columns=84 lines=50
    endif
endfunction


function! BaseSettings()
    set exrc
    set secure

    set nocompatible
    set nobackup
    set nolist
    set noswapfile
    set noshowmatch
    set noundofile
    set notimeout ttimeout ttimeoutlen=0
    set nocursorline
    set nocursorcolumn

    set autoread
    set backspace=indent,eol,start
    set clipboard=unnamed
    set complete+=k
    set cmdheight=2
    set confirm
    set encoding=utf-8
    set fileformat=unix
    set foldnestmax=3
    set hidden
    set hlsearch
    set listchars=nbsp:%,tab:>-,eol:<,trail:-
    set laststatus=2
    set mouse=a
    set matchpairs+=<:>,=:;
    set numberwidth=5
    set number
    set modelines=0
    set ruler
    set relativenumber
    set scrolloff=3
    set splitright
    set showtabline=2
    set showcmd
    set showbreak=>\
    set smartcase
    set shortmess=aI
    set ttymouse=xterm
    set writebackup
    set wildmenu
    set wildignore+=*.pyc,*.zip,*.gz,*.bz,*.tar,*.jpg,*.png,*.gif,*.avi,*.wmv,*.ogg,*.mp3,*.mov
    set wildmode=list:longest,full
    if version > 740
        if has('python')
            set pyx=2
        elseif has('python3')
            set pyx=3
        endif
    endif
endfunction


function! KeyMappings()
    map <leader>. :Sexplore<cr>
    " nore Nonrecursive mapping
    cmap <leader>p <C-R>+
    " Normale Mode
    nnoremap ; :
    nnoremap <leader>p "+p
    nnoremap <leader>d :lcd %:p:h<CR>
    nnoremap <leader>s :%s/<c-r><c-w>//g<left><left>
    nnoremap <leader>v V`]
    nnoremap <leader>" viw<esc>i"<esc>hbi"<esc>lel
    nnoremap <leader>' viw<esc>i'<esc>hbi'<esc>lel
    nnoremap - ddp
    nnoremap _ ddkP
    nnoremap <c-u> <s-v>U
    nnoremap <Leader>fj :%!python -m json.tool<Enter>
    nnoremap <Leader>pd :%!python -m pydoc<space>
    nnoremap <Leader>jl :!npm run lint<cr>
    nnoremap v <c-v>
    " Insert Mode
    inoremap jk <Esc>
    inoremap <c-d> <esc>ddi
    " Visual Mode
    vnoremap <leader>y "+y
    vnoremap \ U
    vnoremap " xi"<esc>pa"<esc>
    vnoremap ' xi'<esc>pa'<esc>


    function! Buffers()
        nnoremap <silent> [b :bprevious<CR>
        nnoremap <silent> ]b :bnext<CR>
        nnoremap <silent> [B :bfirst<CR>
        nnoremap <silent> ]B :blast<CR>
        nnoremap <Leader>l :ls<CR>:b<space>
    endfunction


    function! FunctionKeys()
        function! VisualNavigation()
            :set list!
            execute('IndentGuidesToggle')
        endfunction
        function! UseFullscreen()
            :set fullscreen!
        endfunction
        function! VisualColumn()
            if &cc == ''
                :set colorcolumn=79
            else
                :set colorcolumn=
            endif
        endfunction
        nnoremap <F1>                   :exec "help ".expand("<cword>")<CR>
        nnoremap <s-F1>                 :helpgrep<space>
        nnoremap <silent><F2>           :call VisualNavigation()<CR>
        nnoremap <silent><F3>           :call VisualColumn()<CR>
        nnoremap <silent><F4>           :call DarkColorscheme()<cr>
        nnoremap <silent><F5>           :call LightColorscheme()<cr>
        nnoremap <silent><F6>           :call Colorscheme()<cr>
        nnoremap <silent><F7>           :call NordColorscheme()<cr>
        nnoremap <silent><F12>          :e $MYVIMRC<CR>
        nnoremap <silent><F11>          :call UseFullscreen()<CR>
    endfunction


    function! Tabs()
        nnoremap <leader>tn :tabnew<cr>
        nnoremap <leader>te :tabedit
        nnoremap <leader>tc :tabclose<cr>
        nnoremap <leader>to :tabonly<cr>
        nnoremap <leader>tm :tabmove

        nnoremap <silent>[t :tabprevious<cr>
        nnoremap <silent>]t :tabnext<cr>
        nnoremap <silent>[T :tabfirst<cr>
        nnoremap <silent>]T :tablast<cr>
    endfunction


    function! Folds()
        nnoremap <space> za
        vnoremap <space> zf
    endfunction


    function! Arrows()
        map <UP> <NOP>
        map <DOWN> <NOP>
        map <LEFT> :prev<cr>
        map <RIGHT> :next<cr>
        inoremap <UP> <NOP>
        inoremap <DOWN> <NOP>
        inoremap <LEFT> <NOP>
        inoremap <RIGHT> <NOP>
        map <DOWN> gj
        map <UP> gk
        imap <DOWN> <ESC>gki
        imap <UP> <ESC>gji
    endfunction


    function! Indents()
        nmap < <<
        nmap > >>
        vmap < <gv
        vmap > >gv
    endfunction


    function! ResetOmniKeywordCompl()
        "http://stackoverflow.com/questions/510503/ctrlspace-for-omni-and-keyword-completion-in-vim
        inoremap <expr> <C-Space> pumvisible() \|\| &omnifunc == '' ?
                    \ "\<lt>C-n>" :
                    \ "\<lt>C-x>\<lt>C-o><c-r>=pumvisible() ?" .
                    \ "\"\\<lt>c-n>\\<lt>c-p>\\<lt>c-n>\" :" .
                    \ "\" \\<lt>bs>\\<lt>C-n>\"\<CR>"
    endfunction


    function! EditVimrc()
        nnoremap <leader>ev :split $MYVIMRC<cr>
        nnoremap <leader>sv :source $MYVIMRC<cr>
    endfunction


    call FunctionKeys()
    call Tabs()
    call Folds()
    call Arrows()
    call Indents()
    call ResetOmniKeywordCompl()
    call Buffers()
    call EditVimrc()
endfunction


function! Vimwiki()
    " vimwiki s
    let wiki_1 = {}
    let wiki_1.path = '$HOME/vimfiles/vimwiki/'

    let wiki_2 = {}
    let wiki_2.path = '$HOME/vimwiki/'

    let g:vimwiki_list = [wiki_1, wiki_2]
endfunction


function! Filetypes()
    function! JsAbbrevs()
        iabbrev func function
        iabbrev forl for (let i = 0; i < ; i++) {<esc>8hi
        iabbrev cl console.log()<esc>i
    endfunction


    function! MyPythonSettings()
        nnoremap <Leader>pl :!pylint %<Enter>
        nnoremap <Leader>fl :!flake8 %<Enter>
        setlocal ff=unix
        setlocal makeprg=python3\ %
        setlocal efm=%C\ %.%#,%A\ \ File\ \"%f\"\\,\ line\ %l%.%#,%Z%[%^\ ]%\\@=%m
        setlocal nolist
        if has('python3')
            setlocal omnifunc=python3complete#Complete
        else
            setlocal omnifunc=pythoncomplete#Complete
        endif
        setlocal shiftwidth=4
        setlocal tabstop=4
        setlocal expandtab
        setlocal softtabstop=4
        setlocal shiftround
        setlocal autoindent
        setlocal textwidth=79
        iabbrev ifmain if __name__ == '__main__':
    endfunction


    function! MyTxtSettings()
        setlocal formatoptions=tcrqn textwidth=80
    endfunction


    function! TypescriptSettings()
        setlocal tabstop=4
        setlocal expandtab
        setlocal shiftwidth=2
        setlocal softtabstop=2
        setlocal ff=unix
        setlocal textwidth=120
        call JsAbbrevs()
    endfunction


    function! JavaScriptSettings()
        setlocal ff=unix
        setlocal makeprg=eslint\ %
        setlocal efm=%A%s
        setlocal copyindent
        setlocal smartindent
        setlocal tabstop=4
        setlocal expandtab
        setlocal shiftwidth=2
        setlocal softtabstop=2
        setlocal textwidth=90
        call JsAbbrevs()
    endfunction


    function! HTMLSettings()
        setlocal ff=unix
        setlocal copyindent
        setlocal smartindent
        setlocal tabstop=4
        setlocal expandtab
        setlocal shiftwidth=2
        setlocal softtabstop=2
    endfunction


    function! DartSettings()
        setlocal copyindent
        setlocal smartindent
        setlocal tabstop=4
        setlocal expandtab
        setlocal shiftwidth=2
        setlocal softtabstop=2
        setlocal textwidth=120
    endfunction


    function! YamlSettings()
        call Indent()
    endfunction


    function! Rust_Settings()
        setlocal makeprg=rustc\ %
        call Indent()
    endfunction


    function! C_Settings()
        setlocal ff=unix
        setlocal textwidth=79
        call Indent()
    endfunction


    function! Wiki_Settings()
        setlocal textwidth=79
    endfunction


    function! MarkdownSettings()
        setlocal ff=unix
        setlocal wrap
        setlocal textwidth=79
    endfunction


    au FileType txt                     call MyTxtSettings()
    au FileType py,python               call MyPythonSettings()
    au FileType ts,tsc,typescript       call TypescriptSettings()
    au FileType json,js,javascript      call JavaScriptSettings()
    au FileType html                    call HTMLSettings()
    au FileType dart                    call DartSettings()
    au FileType yaml                    call YamlSettings()
    au FileType c,cpp                   call C_Settings()
    au FileType vimwiki                 call Wiki_Settings()
    au FileType markdown                call MarkdownSettings()

endfunction


function! Abbrevs()
    iabbrev @@ dev@visualisierte.de
    iabbrev ccopy Copyright 2017 Marcus Kammer, all rights reserved.
    iabbrev waht what
    iabbrev tehn then
    iabbrev adn and
    iabbrev uw Update wiki
endfunction


function! UserCommands()
    :command Fjs !fixjsstyle %
    :command Jsb !js-beautify -r %
    :command Ap8 !autopep8 --in-place --aggressive --aggressive %
    :command Pep8 !pep8 %
    :command Pd !pydoc
endfunction


function! StandardStatusLine()
    set statusline=%f
    " The following items will be aligned right
    set statusline+=%=
    " Show filetype
    set statusline+=%y
    set statusline+=--
    " Show current line
    set statusline+=%l
    set statusline+=//
    " Show all lines
    set statusline+=%L
    set statusline+=--
    " Show window width
    set statusline+=%{winwidth(0)}
    set statusline+=//
    " Show window height
    set statusline+=%{winheight(0)}
    set statusline+=--
    set statusline+=%{hostname()}
endfunction


function! NewStatusLine()
    set laststatus=2
    set statusline=
    set statusline+=\ «
    set statusline+=\ %t
    set statusline+=\ [%n%H%M%R%W%Y]
    set statusline+=\ »\ %*
    set statusline+=%#keyword#\ %*
    set statusline+=%<
    set statusline+=%#keyword#\ %F
    set statusline+=%=
    set statusline+=\ «
    set statusline+=\ %L
    set statusline+=\ %{winwidth(0)}
    set statusline+=\ %{winheight(0)}
    set statusline+=\ %{hostname()}
    set statusline+=\ %{strftime(\"%H:%M\")}
    set statusline+=\ »
endfunction


call Autocommands()
call SetRtp()
call BaseSettings()
call Indent()
call Colorscheme()
call Vimwiki()
call Filetypes()
call UserCommands()
call Abbrevs()
call KeyMappings()
call NewStatusLine()
if has('gui_running')
    call GUI()
endif
if has("gui_win32")
    call WindowsSettings()
endif
if hostname() == 'ares.local'
endif
